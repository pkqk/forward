require 'image'
require 'treetop'

# Treetop grammar is loaded below after this module

module ImageManipulation
  ImageRequired = Class.new(StandardError)
  Terminate = Class.new(StandardError)

  class Command < Treetop::Runtime::SyntaxNode
  end
  
  class RequiresImage < Command
    
    def do(image)
      if image
        alter(image)
      else
        raise ImageRequired.new("start an image first with 'I :width :height'")
      end
      image
    end
  end

  class Init < Command
    def do(image)
      Image.new(width, height)
    end
  end

  class Show < RequiresImage
    def alter(image)
      puts
      puts "=>"
      puts image
      puts
    end
  end

  class Clear < RequiresImage
    def alter(image)
      image.clear
    end
  end

  class Exit < Command
    def do(nothing)
      raise Terminate.new
    end
  end

  class LightUp < RequiresImage
    def alter(image)
      image[x,y] = colour
    end
  end

  class VerticalLine < RequiresImage
    def alter(image)
      image.vertical_line(x, start_y, end_y, colour)
    end
  end

  class HorizontalLine < RequiresImage
    def alter(image)
      image.horizontal_line(y, start_x, end_x, colour)
    end
  end

  class Fill < RequiresImage
    def alter(image)
      image.fill(x, y, colour)
    end
  end
end

Treetop.load File.join(File.dirname(__FILE__), 'image_manipulation.treetop')
