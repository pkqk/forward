# encoding: utf-8

class Image
  PointError = Class.new(StandardError)
  SizeError = Class.new(StandardError)
  attr_accessor :width, :height

  def initialize(width, height)
    @width, @height = width.to_i, height.to_i
    unless (1..250).include?(@width) && (1..250).include?(@height)
      raise SizeError.new("#{@width}✕#{@height} is bigger than the arbirary size of 250")
    end
    clear
  end
  
  def clear(colour='O')
    @image = colour.to_s*width*height
  end

  def [](x, y)
    @image[pos(x,y)]
  end

  def []=(x, y, colour)
    @image[pos(x,y)] = colour.to_s
  end
  
  def in_hrange?(x)
    (1..width).include?(x.to_i)
  end
  
  def in_vrange?(y)
    (1..height).include?(y.to_i)
  end
  
  def pos(x, y)
    x,y = x.to_i, y.to_i
    raise PointError.new("Bad x co-ord in (#{x},#{y})") unless in_hrange?(x)
    raise PointError.new("Bad y co-ord in (#{x},#{y})") unless in_vrange?(y)
    (y-1)*width+(x-1)
  end

  def to_s
    (1..height).map { |y| @image[pos(1,y)..pos(width,y)]}.join("\n")
  end

  def vertical_line(x, start_y, end_y, colour)
    start_y, end_y = end_y, start_y if start_y > end_y
    start_y.to_i.upto(end_y.to_i) do |y|
      self[x,y] = colour.to_s
    end
  end

  def horizontal_line(y, start_x, end_x, colour)
    start_x, end_x = end_x, start_x if start_x > end_x
    start_x.to_i.upto(end_x.to_i) do |x|
      self[x,y] = colour.to_s
    end
  end

  def fill(x,y, colour)
    flood(x.to_i, y.to_i, self[x,y], colour.to_s)
  end
  
  def flood(x, y, original_colour, colour)
    return unless self[x,y] == original_colour
    self[x,y] = colour
    flood(x-1, y, original_colour, colour) if in_hrange?(x-1)
    flood(x+1, y, original_colour, colour) if in_hrange?(x+1)
    flood(x, y-1, original_colour, colour) if in_vrange?(y-1)
    flood(x, y+1, original_colour, colour) if in_vrange?(y+1)
  end
  
  def ==(other_image)
    return false unless other_image.is_a?(Image)
    other_image.to_s == to_s
  end
end