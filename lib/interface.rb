require 'image_manipulation'

class Interface
  def prompt
    if input = Readline.readline('> ', true)
      input.strip
    end
  end

  def run
    imp = ImageManipulationParser.new
    while input = prompt
      begin
        if command = imp.parse(input)
          @image = command.do(@image)
        else
          puts "error: #{imp.failure_reason}"
        end
      rescue ImageManipulation::Terminate
        break
      rescue => e
        puts "error: #{e.message}"
      end
    end
  end
end
