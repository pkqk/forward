require 'image_manipulation'

describe ImageManipulation do
  def get(input)
    ImageManipulationParser.new.parse(input)
  end
  
  describe ImageManipulation::Init do
    subject { get("I 5 10") }

    its("width.to_i") { should == 5 }
    its("height.to_i") { should == 10 }

    it 'should return a new image' do
      subject.do(nil).should == Image.new(5,10)
    end
  end

  describe ImageManipulation::Clear do
    subject { get("C") }

    it 'should clear the image' do
      image = double('image')
      image.should_receive(:clear)
      subject.do(image)
    end
  end

  describe ImageManipulation::Exit do
    subject { get("X") }

    it 'should raise a termination exception' do
      lambda { subject.do(nil) }.should raise_error(ImageManipulation::Terminate)
    end
  end

  describe ImageManipulation::LightUp do
    subject { get("L 10 5 B") }

    its("x.to_i") { should == 10 }
    its("y.to_i") { should == 5 }
    its("colour.to_s") { should == "B" }
    
    it 'should set the pixel' do
      image = double('image')
      image.should_receive(:[]=).with(subject.x, subject.y, subject.colour)
      subject.do(image)
    end
  end

  describe ImageManipulation::VerticalLine do
    subject { get("V 3 10 5 C") }

    its("x.to_i") { should == 3 }
    its("start_y.to_i") { should == 10 }
    its("end_y.to_i") { should == 5 }
    its("colour.to_s") { should == "C" }
    
    it 'should set the pixel' do
      image = double('image')
      image.should_receive(:vertical_line).with(subject.x, subject.start_y, subject.end_y, subject.colour)
      subject.do(image)
    end
  end

  describe ImageManipulation::HorizontalLine do
    subject { get("H 1 12 20 F") }

    its("y.to_i") { should == 1 }
    its("start_x.to_i") { should == 12 }
    its("end_x.to_i") { should == 20 }
    its("colour.to_s") { should == "F" }
    
    it 'should set the pixel' do
      image = double('image')
      image.should_receive(:horizontal_line).with(subject.y, subject.start_x, subject.end_x, subject.colour)
      subject.do(image)
    end
  end

  describe ImageManipulation::Fill do
    subject { get("F 10 15 H") }

    its("x.to_i") { should == 10 }
    its("y.to_i") { should == 15 }
    its("colour.to_s") { should == "H" }
    
    it 'should set the pixel' do
      image = double('image')
      image.should_receive(:fill).with(subject.x, subject.y, subject.colour)
      subject.do(image)
    end
  end
end
