require 'image'

describe Image do
  def expected_image(*lines)
    lines.join("\n")
  end

  describe 'creation' do
    subject { Image.new(5,5).to_s }

    its(:length) { should == 5*5+4 }

    it 'be all O to start with' do
      subject.should == expected_image("OOOOO",
                                       "OOOOO",
                                       "OOOOO",
                                       "OOOOO",
                                       "OOOOO")
    end
  end
  
  describe 'pixel accessors' do
    subject { Image.new(5,5) }

    it 'sets pixels at a location' do
      subject[2,2] = "W"
      subject.to_s.should == expected_image("OOOOO",
                                            "OWOOO",
                                            "OOOOO",
                                            "OOOOO",
                                            "OOOOO")
    end

    it 'should be 1 indexed' do
      subject[1,1] = 'A'
      subject[5,1] = 'B'
      subject[5,5] = 'C'
      subject[1,5] = 'D'
      subject.to_s.should == expected_image("AOOOB",
                                            "OOOOO",
                                            "OOOOO",
                                            "OOOOO",
                                            "DOOOC")
    end
  end

  describe 'line drawing' do
    subject { Image.new(5,5) }

    it 'should draw a horizontal line' do
      subject.horizontal_line(3,1,5,'C')
      subject.to_s.should == expected_image("OOOOO",
                                            "OOOOO",
                                            "CCCCC",
                                            "OOOOO",
                                            "OOOOO")
    end

    it 'should draw a vertical line' do
      subject.vertical_line(3,1,5,'C')
      subject.to_s.should == expected_image("OOCOO",
                                            "OOCOO",
                                            "OOCOO",
                                            "OOCOO",
                                            "OOCOO")
    end

    it 'should draw the vertical line from bottom to top if end value is smaller' do
      subject.vertical_line(2,5,1,'C')
      subject.to_s.should == expected_image("OCOOO",
                                            "OCOOO",
                                            "OCOOO",
                                            "OCOOO",
                                            "OCOOO")
    end

    it 'should draw the horizontal line from right to left if end value is smaller' do
      subject.horizontal_line(4,5,1,'T')
      subject.to_s.should == expected_image("OOOOO",
                                            "OOOOO",
                                            "OOOOO",
                                            "TTTTT",
                                            "OOOOO")
    end
  end

  describe 'filling' do
    subject { Image.new(10,10) }
  
    it 'should fill an enclosed space' do
      subject.vertical_line(2,2,7,'B')
      subject.horizontal_line(2,3,10,'B')
      subject[9,3] = subject[8,4] = subject[7,5] = 'B'
      subject[6,6] = subject[5,7] = subject[4,8] = 'B'
      subject[3,8] = 'B'
      subject.fill(4,4,'S')
      subject.to_s.should == expected_image("OOOOOOOOOO",
                                            "OBBBBBBBBB",
                                            "OBSSSSSSBO",
                                            "OBSSSSSBOO",
                                            "OBSSSSBOOO",
                                            "OBSSSBOOOO",
                                            "OBSSBOOOOO",
                                            "OOBBOOOOOO",
                                            "OOOOOOOOOO",
                                            "OOOOOOOOOO")
    end
  end
end
